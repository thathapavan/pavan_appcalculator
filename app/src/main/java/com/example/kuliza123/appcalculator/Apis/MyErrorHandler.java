package com.example.kuliza123.appcalculator.Apis;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ggoyal on 5/28/15.
 */
public class MyErrorHandler implements ErrorHandler {

    @Override public Throwable handleError(RetrofitError cause) {
        Response r = cause.getResponse();
//        if (r != null && r.getStatus() == 401) {
//            return new UnauthorizedException(cause);
//        }
        return cause;
    }
}