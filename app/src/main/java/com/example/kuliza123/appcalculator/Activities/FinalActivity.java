package com.example.kuliza123.appcalculator.Activities;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kuliza123.appcalculator.Apis.RestClientFinal;
import com.example.kuliza123.appcalculator.Fragments.EstimateFragment;
import com.example.kuliza123.appcalculator.Models.Data;
import com.example.kuliza123.appcalculator.Models.PlatformListModel;
import com.example.kuliza123.appcalculator.Models.Questions;
import com.example.kuliza123.appcalculator.R;
import com.example.kuliza123.appcalculator.Utils.Constants;
import com.example.kuliza123.appcalculator.Utils.DialogUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FinalActivity extends AppCompatActivity implements  EstimateFragment.OnButtonClickFragmentInteraction,
        EstimateFragment.OnBackButtonClickFragmentInteraction {
    private Bundle bundle;
    private HashMap<String,ArrayList<PlatformListModel>> mHashMap;
    private int mTotalTime,mTotalCost;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);
        setupToolbar();
        bundle = getIntent().getExtras().getBundle(Constants.finalBundleKey);
        EstimateFragment estimateFragment = new EstimateFragment();
        estimateFragment.setArguments(bundle);

        mHashMap = (HashMap<String, ArrayList<PlatformListModel>>) bundle.getSerializable(Constants.OptionsMap);
        // Logger.d(mHashMap.toString());
        mTotalTime = bundle.getInt(Constants.TIME_KEY);
        mTotalCost = mTotalTime * Constants.hourlyWage;
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameFinal,estimateFragment);
        fragmentTransaction.commit();
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textView = (TextView)toolbar.findViewById(R.id.toolbar_text);
        ImageView back = (ImageView) toolbar.findViewById(R.id.back_button_left);
        final Button button = (Button)toolbar.findViewById(R.id.btnHome);
        Button buttonLeft = (Button) toolbar.findViewById(R.id.btnHomeLeft);
        buttonLeft.setVisibility(View.GONE);
        button.setVisibility(View.VISIBLE);
        back.setVisibility(View.VISIBLE);
        back.setImageResource(R.drawable.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.onHomeIconPressed(FinalActivity.this, "", 0.9, 0.35, true, true);
            }
        });
        textView.setText(Constants.YOUR_ESTIMATE);
        toolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_final, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClick(String email) {
        /*Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"pavan.nagaraju@kuliza.com",email.toString()});
        i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
        i.putExtra(Intent.EXTRA_TEXT, "Email Id of the client: " + email);
        i.putExtra(Intent.ACTION_SEND_MULTIPLE, true);
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }*/

        RestClientFinal restClientFinal = new RestClientFinal();
        Questions questions = createQuestionsAns(email);
        restClientFinal.getApiService().sumbitData(questions);
    }

    private Questions createQuestionsAns(String email) {
        Questions questions = new Questions();
        questions.setTotalCost(mTotalCost);
        questions.setTotalTime(mTotalTime);
        questions.setEmail(email);
        Data data = new Data();
        data.setQ0(getSelectedOPtions(mHashMap.get(Constants.KEY[0])));
        data.setQ1(getSelectedOPtions(mHashMap.get(Constants.KEY[1])));
        data.setQ2(getSelectedOPtions(mHashMap.get(Constants.KEY[2])));
        data.setQ3(getSelectedOPtions(mHashMap.get(Constants.KEY[3])));
        data.setQ4(getSelectedOPtions(mHashMap.get(Constants.KEY[4])));
        data.setQ5(getSelectedOPtions(mHashMap.get(Constants.KEY[5])));
        data.setQ6(getSelectedOPtions(mHashMap.get(Constants.KEY[6])));
        data.setQ7(getSelectedOPtions(mHashMap.get(Constants.KEY[7])));
        data.setQ8(getSelectedOPtions(mHashMap.get(Constants.KEY[8])));
        data.setQ9(getSelectedOPtions(mHashMap.get(Constants.KEY[9])));
        data.setQ10(getSelectedOPtions(mHashMap.get(Constants.KEY[10])));
        data.setQ11(getSelectedOPtions(mHashMap.get(Constants.KEY[11])));
        data.setQ12(getSelectedOPtions(mHashMap.get(Constants.KEY[12])));
        data.setQ13(getSelectedOPtions(mHashMap.get(Constants.KEY[13])));
        data.setQ14(getSelectedOPtions(mHashMap.get(Constants.KEY[14])));
        questions.setData(data);
        return questions;
    }
    private String getSelectedOPtions(ArrayList<PlatformListModel> mList){
        String string = "";
        boolean flag=false;
        for(int i=0;i<mList.size();i++){
            if(mList.get(i).getIsSelected()){
                if(flag == false){
                    string = string+mList.get(i).getName();
                }
                else{
                    string = string  + mList.get(i).getName()+",";
                    flag = true;
                }
            }
        }
        return string;
    }

    @Override
    public void onLearnMoreClick() {
        String url = "http://www.kuliza.com/";
        final Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
        startActivity(intent);
    }

    @Override
    public void OnBackButtonClick() {
        /*mButtonsView.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.VISIBLE);
        fabNext.setVisibility(View.VISIBLE);
        mToolbar.findViewById(R.id.btnHome).setVisibility(View.GONE);
        mToolbar.findViewById(R.id.btnHomeLeft).setVisibility(View.VISIBLE);
        mToolbar.findViewById(R.id.back_button_left).setVisibility(View.GONE);
        //onFragmentInteraction();
        getFragmentManager().popBackStack();
        */
        onBackPressed();
    }
    public void onHomeClick(){
        Intent intent = new Intent(FinalActivity.this,HomeScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
