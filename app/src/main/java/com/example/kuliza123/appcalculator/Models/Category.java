package com.example.kuliza123.appcalculator.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by kuliza214 on 3/8/15.
 */
public class Category implements Parcelable{
    public ArrayList<PlatformListModel> mList;
    public Category(){
        mList = new ArrayList<PlatformListModel>();
    }

    protected Category(Parcel in) {
        mList = in.createTypedArrayList(PlatformListModel.CREATOR);
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    /*public Category(Parcel parcel){
            this.mList = parcel.readArrayList(null);
        }*/
    public void addItem(PlatformListModel platformListModel){
        mList.add(platformListModel);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mList);
    }
}
