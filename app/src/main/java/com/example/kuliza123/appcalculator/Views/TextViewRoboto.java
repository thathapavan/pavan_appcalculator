package com.example.kuliza123.appcalculator.Views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.example.kuliza123.appcalculator.R;

/**
 * Created by kuliza214 on 27/8/15.
 */
public class TextViewRoboto extends TextView{
    public TextViewRoboto(Context context) {
        super(context);
        Typeface face=Typeface.createFromAsset(context.getAssets(), "Roboto-Thin.ttf");
        this.setTypeface(face);
    }

    public TextViewRoboto(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face=Typeface.createFromAsset(context.getAssets(), "Roboto-Thin.ttf");
        this.setTypeface(face);
    }

    public TextViewRoboto(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Typeface face=Typeface.createFromAsset(context.getAssets(), "Roboto-Thin.ttf");
        this.setTypeface(face);
    }

    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);
    }}
