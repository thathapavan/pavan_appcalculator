package com.example.kuliza123.appcalculator.Models;

/**
 * Created by kuliza214 on 9/9/15.
 */

public class Questions {

    private String email;

    private Integer totalTime;

    private Integer totalCost;
    private Data data;

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The totalTime
     */
    public Integer getTotalTime() {
        return totalTime;
    }

    /**
     *
     * @param totalTime
     * The total_time
     */
    public void setTotalTime(Integer totalTime) {
        this.totalTime = totalTime;
    }

    /**
     *
     * @return
     * The totalCost
     */
    public Integer getTotalCost() {
        return totalCost;
    }

    /**
     *
     * @param totalCost
     * The total_cost
     */
    public void setTotalCost(Integer totalCost) {
        this.totalCost = totalCost;
    }

    /**
     *
     * @return
     * The data
     */
    public Data getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(Data data) {
        this.data = data;
    }

}
