package com.example.kuliza123.appcalculator.Apis;

import com.example.kuliza123.appcalculator.Models.Questions;

import retrofit.http.FormUrlEncoded;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by kuliza214 on 9/9/15.
 */
public interface ApiService {
    @FormUrlEncoded
    @POST("/submit/")
    void sumbitData(Questions questions);
}
