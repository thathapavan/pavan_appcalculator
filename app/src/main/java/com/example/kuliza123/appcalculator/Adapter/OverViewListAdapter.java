package com.example.kuliza123.appcalculator.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.preference.PreferenceActivity;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kuliza123.appcalculator.Activities.MainActivity;
import com.example.kuliza123.appcalculator.Fragments.EstimateFragment;
import com.example.kuliza123.appcalculator.Models.PlatformListModel;
import com.example.kuliza123.appcalculator.R;
import com.example.kuliza123.appcalculator.Utils.Constants;
import com.example.kuliza123.appcalculator.Utils.Logger;

import java.security.PublicKey;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by kuliza214 on 24/8/15.
 */
public class OverViewListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private boolean mCollapced = true;
    private int mHeader =1,mSelectedList =2 ,mFooter =3;
    private Context mContext;
    private HashMap<String,ArrayList<PlatformListModel>> mHashMap;
    private int mTotalTime;
    private View mRootView;
    private OnSubmitClick mListner;
    private HeaderHolder mHeaderHolder;
    private FooterHolder mFooterHolder;
    private ListHolder mListHolder;
    private int lastPosition;

    public OverViewListAdapter(Context context,EstimateFragment estimateFragment, HashMap<String,ArrayList<PlatformListModel>> hashMap,int totalTime,View rootView){
        mContext = context;
        mHashMap = hashMap;
        mTotalTime = totalTime;
        mRootView = rootView;
        mListner = (OnSubmitClick) estimateFragment;
        //setHasStableIds(true);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == mSelectedList) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.selected_list_item, parent, false);
            ListHolder holder = new ListHolder(view);
            return holder;
        }
        else if(viewType == mHeader){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header,parent,false);
            HeaderHolder headerHolder = new HeaderHolder(view);
            return headerHolder;
        }
        else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.footer, parent, false);
            FooterHolder footerHolder  = new FooterHolder(view);
            return footerHolder;
        }
    }

    /*@Override
    public long getItemId(int position)
    {
        return position; 
    }*/
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position == 0) {
            mHeaderHolder = (HeaderHolder) holder;
            mHeaderHolder.mTimeEstimate.setText(mTotalTime + " hrs");
            String totlaAmount = NumberFormat.getIntegerInstance(new Locale("EN", "US")).format(mTotalTime * Constants.hourlyWage);
            mHeaderHolder.mCosEstimate.setText("$ " + totlaAmount);
        }
        else if (position == getLastRowCount()) {
           mFooterHolder = (FooterHolder) holder;
            mFooterHolder.mExpandList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mCollapced){
                        mFooterHolder.mExpandList.setText(Constants.collapse_the_list);
                        mCollapced = false;
                        notifyItemRangeInserted(1,15);
                    }
                    else{
                        mFooterHolder.mExpandList.setText(Constants.viewCompleateList);
                        mCollapced = true;
                        notifyItemRangeRemoved(1,15);
                    }
                }
            });
            mFooterHolder.mSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CharSequence email = (CharSequence) mFooterHolder.mEditTextEmail.getText();
                    if(email.equals("")){
                        Snackbar.make(mRootView, "Please enter your EmailId", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                    if(isValidEmail(email)){
                        mListner.onClick(email.toString());
                        mFooterHolder.mEditTextEmail.setText("");
                        Snackbar.make(mRootView, "Valid Email", Snackbar.LENGTH_LONG).show();
                    }
                    else {
                        mFooterHolder.mEditTextEmail.setText("");
                        Snackbar.make(mRootView,"Enter Valid Email Address",Snackbar.LENGTH_LONG).show();
                    }
                }
            });
            mFooterHolder.mLearnMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListner.onLearnMoreClick();
                }
            });
        }
        else {
            ArrayList<PlatformListModel> list = mHashMap.get(Constants.KEY[position-1]);
            mListHolder = (ListHolder) holder;
            mListHolder.mHeading.setText(Constants.headings[position-1]);
            int counter = 0;
            for(int i=0;i<list.size();i++){
                if(list.get(i).getIsSelected()){
                    int imageView = list.get(i).getSelectedImage();
                    String name = list.get(i).getName();
                    mListHolder.mImages.get(counter).setVisibility(View.VISIBLE);
                    mListHolder.mTextViews.get(counter).setVisibility(View.VISIBLE);
                    mListHolder.mImages.get(counter).setImageResource(imageView);
                    mListHolder.mTextViews.get(counter).setText(name);
                    counter++;
                }
            }
            for(int i=counter;i<mListHolder.mImages.size();i++){
                /*mListHolder.mImages.get(i).setImageResource(R.drawable.black_button);
                mListHolder.mTextViews.get(i).setText("");*/
                mListHolder.mImages.get(i).setVisibility(View.GONE);
                mListHolder.mTextViews.get(i).setVisibility(View.GONE);
            }
            if(position == 1){
                mListHolder.mDummyView.setVisibility(View.GONE);
            }
            else {
                mListHolder.mDummyView.setVisibility(View.VISIBLE);
            }
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
    @Override
    public int getItemCount() {
        Logger.d("Items Count:-"+(Constants.total_questions + 2));
        if(mCollapced){
            return 2;
        }
        return Constants.total_questions + 2;
    }

    @Override
    public int getItemViewType(int position) {
        int type=0;
        EstimateFragment estimateFragment= new EstimateFragment();

        if(position == 0){
            return mHeader;
        }
        else if(position == getLastRowCount()){
            return mFooter;
        }
        return mSelectedList;
    }

    public int getLastRowCount() {

        return getItemCount() - 1;
    }

    public class HeaderHolder extends  RecyclerView.ViewHolder{
        private RelativeLayout mHeader;
        private TextView mTimeEstimate,mCosEstimate;
        public HeaderHolder(View itemView) {
            super(itemView);
            mHeader = (RelativeLayout) itemView.findViewById(R.id.rlHeader);
            mTimeEstimate  = (TextView) itemView.findViewById(R.id.tvTimeEstimate);
            mCosEstimate = (TextView) itemView.findViewById(R.id.tvCostEstimate);
        }
    }

    public class FooterHolder extends RecyclerView.ViewHolder{
        private RelativeLayout mFooter;
        private TextView mExpandList,mLearnMore,mSubmit,mEditTextEmail;
        public FooterHolder(View itemView){
            super(itemView);
            mFooter = (RelativeLayout) itemView.findViewById(R.id.footerLayout);
            mExpandList = (TextView) itemView.findViewById(R.id.btnViewCompleateList);
            mLearnMore = (TextView) itemView.findViewById(R.id.tvLearnMore);
            mSubmit = (TextView) itemView.findViewById(R.id.btnSubmit);
            mEditTextEmail = (EditText) itemView.findViewById(R.id.edtEmailId);
        }
    }

    public class ListHolder extends RecyclerView.ViewHolder{
        public TextView mHeading;
        public ArrayList<ImageView> mImages;
        public ArrayList<TextView> mTextViews;
        public View mDummyView;
        public ListHolder(View view) {
            super(view);
            mHeading = (TextView)view.findViewById(R.id.tvHeading);
            mDummyView = (View) view.findViewById(R.id.dummyView);
            mImages = new ArrayList<>();
            mTextViews = new ArrayList<>();

            mImages.add((ImageView)view.findViewById(R.id.imgImage1));
            mImages.add((ImageView)view.findViewById(R.id.imgImage2));
            mImages.add((ImageView)view.findViewById(R.id.imgImage3));
            mImages.add((ImageView)view.findViewById(R.id.imgImage4));
            mImages.add((ImageView)view.findViewById(R.id.imgImage5));

            mTextViews.add((TextView)view.findViewById(R.id.tvPlatform1));
            mTextViews.add((TextView)view.findViewById(R.id.tvPlatform2));
            mTextViews.add((TextView)view.findViewById(R.id.tvPlatform3));
            mTextViews.add((TextView)view.findViewById(R.id.tvPlatform4));
            mTextViews.add((TextView)view.findViewById(R.id.tvPlatform5));
        }
    }
    public interface OnSubmitClick{
        public void onClick(String string);
        public void onLearnMoreClick();
    }
}