package com.example.kuliza123.appcalculator.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.kuliza123.appcalculator.R;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntroFragment;

public class SplashScreen extends AppIntro2 {

    @Override
    public void init(Bundle savedInstanceState) {
        // Add your slide's fragments here
        // AppIntro will automatically generate the dots indicator and buttons.
        /*addSlide(first_fragment);
        addSlide(second_fragment);
        addSlide(third_fragment);
        addSlide(fourth_fragment);*/

        // Instead of fragments, you can also use our default slide
        // Just set a title, description, background and image. AppIntro will do the rest
        addSlide(AppIntroFragment.newInstance("First Screen", "First Screen Message", R.drawable.first, 0));
        addSlide(AppIntroFragment.newInstance("Second Screen", "Second Screen Message", R.drawable.second, 0));
        addSlide(AppIntroFragment.newInstance("Third Screen", "Third Screen Message", R.drawable.third, 0));
        addSlide(AppIntroFragment.newInstance("Fourth Screen", "Fourth Screen Message", R.drawable.fourth, 0));

        // OPTIONAL METHODS
        // Override bar/separator color
        /*setBarColor(Color.parseColor("#3F51B5"));
        setSeparatorColor(Color.parseColor("#2196F3"));*/

        // Hide Skip/Done button
        //showSkipButton(true);
        showDoneButton(true);

        // Turn vibration on and set intensity
        // NOTE: you will probably need to ask VIBRATE permesssion in Manifest
        //setVibrate(true);
        //setVibrateIntensity(30);
    }

/*    @Override
    public void onSkipPressed() {
        onBackPressed();
        // Do something when users tap on Skip button.
    }*/

    @Override
    public void onDonePressed() {
        onBackPressed();
        // Do something when users tap on Done button.
    }
}
